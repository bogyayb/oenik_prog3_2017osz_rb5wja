﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Toilet_Pipe
{
    class PipeToRectangleGeometryConverter : IValueConverter
    {
       
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //be: pipe
            //ki: RectangleGeometry
            Pipe pipe = (Pipe)value;

            Rect pipeRect = new Rect(pipe.Helyzet.X, pipe.Helyzet.Y, Pipe.Szelesseg, Pipe.Magassag);
            RectangleGeometry pipeRectangleGeometry = new RectangleGeometry(pipeRect);

            return pipeRectangleGeometry;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
