﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Toilet_Pipe
{
    public class MegoldasResz : Bindable
    {
        //fields
        int x;
        int y;
        List<int> adottAllasok;

        //ctor
        public MegoldasResz(int x, int y, List<int> allasok)
        {
            this.X = x;
            this.Y = y;
            this.AdottAllasok = allasok;
        }

        //get/set
      
        public List<int> AdottAllasok
        {
            get
            {
                return adottAllasok;
            }

            set
            {
                adottAllasok = value;
                OPC();
            }
        }

        public int X
        {
            get
            {
                return x;
            }

            set
            {
                x = value;
            }
        }

        public int Y
        {
            get
            {
                return y;
            }

            set
            {
                y = value;
            }
        }
    }
}
