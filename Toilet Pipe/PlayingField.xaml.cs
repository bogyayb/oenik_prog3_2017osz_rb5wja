﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Toilet_Pipe
{
    public partial class PlayingField : Window
    {
        //fields
        BusinessLogic BL;

        public PlayingField(BusinessLogic BL)
        {
            InitializeComponent();
            BL.Ido = BL.AlapIdo;
            
            if (BL.Lvl == 0 || BL.Lvl == 1)
            {
                BL.Betolt("level1", 100);
            }
            BL.Dt.Start();
            this.BL = BL;
                        
            this.DataContext = BL;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            BL.Dt.Stop();
        }

        private void MouseMove(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Hand;
        }

        private void MouseLeave(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //lekérjük az egér koordinátáit az Imagunkhoz vizsonyítva, amiben a csövek vannak
            Point p = e.GetPosition(image);
            double x = p.X;
            double y = p.Y;

            BL.Kattintas(x,y);
        }

        private void Label_MouseDown(object sender, MouseButtonEventArgs e)
        {
            image.Visibility = Visibility.Hidden;
            image2.Visibility = Visibility.Visible;

            label_pause.Visibility = Visibility.Hidden;
            BL.Dt.Stop();
        }

        private void label_continue_MouseDown(object sender, MouseButtonEventArgs e)
        {
            image.Visibility = Visibility.Visible;
            image2.Visibility = Visibility.Hidden;

            label_pause.Visibility = Visibility.Visible;
            BL.Dt.Start();
        }

        //ELLENŐRZÉS
        private void Label_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (BL.MegoldasEllenorzes())
            {
                if (BL.Lvl == 1)
                {
                    BL.Lvl++;
                    MessageBoxResult x = MessageBox.Show("Level 1 telejsítve, már csak 3 pálya van hátra!", "Gratulálunk!", MessageBoxButton.OK, MessageBoxImage.Information);
                    BL.Betolt("level2", 90);
                    BL.MegoldasBetolt("level2_megoldas");
                    BL.Ido = BL.AlapIdo;

                    PlayingField playingField = new PlayingField(this.BL);
                    this.Close();
                    playingField.ShowDialog();
                }
                else if (BL.Lvl == 2)
                {
                    BL.Lvl++;
                    MessageBoxResult x = MessageBox.Show("Level 2 telejsítve, már csak 2 pálya van hátra!", "Gratulálunk!", MessageBoxButton.OK, MessageBoxImage.Information);
                    BL.Betolt("level3", 80);
                    BL.MegoldasBetolt("level3_megoldas");
                    BL.Ido = BL.AlapIdo;

                    PlayingField playingField = new PlayingField(this.BL);
                    this.Close();
                    playingField.ShowDialog();
                }
                else if (BL.Lvl == 3)
                {
                    BL.Lvl++;
                    MessageBoxResult x = MessageBox.Show("Level 3 telejsítve, már csak 1 pálya van hátra!", "Gratulálunk!", MessageBoxButton.OK, MessageBoxImage.Information);
                    BL.Betolt("level4", 70);
                    BL.MegoldasBetolt("level4_megoldas");
                    BL.Ido = BL.AlapIdo;

                    PlayingField playingField = new PlayingField(this.BL);
                    this.Close();
                    playingField.ShowDialog();
                }
                else if (BL.Lvl == 4)
                {
                    BL.Dt.Stop();
                    MessageBoxResult x = MessageBox.Show("Szeretnéd menteni az eredményedet?", "Gratulálnuk, teljesítetted a játékot!", MessageBoxButton.OKCancel, MessageBoxImage.Information);
                    if (x == MessageBoxResult.OK)
                    {
                        Player p = BL.Jatekos;
                       
                        BL.Eredmenyek.Add(new Eredmeny(p.Nev, p.Pont.ToString(), p.Eletkor, p.Nehezseg));
                        BL.Dt.Stop();
                        this.Close();

                        Score score = new Score(BL);
                        score.ShowDialog();
                    }
                    else
                        this.Close();
                }

            }
            else
            {

                if (BL.Lvl == 1)
                {
                    MessageBox.Show("Sajnos rosszul kötötted össze a csöveket, kezdheted előről a pályát! :(", "Rossz megoldás!", MessageBoxButton.OK, MessageBoxImage.Error);
                    BL.Betolt("level1", 100);
                    BL.MegoldasBetolt("level1_megoldas");
                    BL.Ido = BL.AlapIdo;
                }
                else if (BL.Lvl == 2)
                {
                    MessageBox.Show("Sajnos rosszul kötötted össze a csöveket, kezdheted előről a pályát! :(", "Rossz megoldás!", MessageBoxButton.OK, MessageBoxImage.Error);
                    BL.Betolt("level2", 90);
                    BL.MegoldasBetolt("level2_megoldas");
                    BL.Ido = BL.AlapIdo;
                }
                else if (BL.Lvl == 3)
                {
                    MessageBox.Show("Sajnos rosszul kötötted össze a csöveket, kezdheted előről a pályát! :(", "Rossz megoldás!", MessageBoxButton.OK, MessageBoxImage.Error);
                    BL.Betolt("level3", 80);
                    BL.MegoldasBetolt("level3_megoldas");
                    BL.Ido = BL.AlapIdo;
                }
                else if (BL.Lvl == 4)
                {
                    MessageBox.Show("Sajnos rosszul kötötted össze a csöveket, kezdheted előről a pályát! :(", "Rossz megoldás!", MessageBoxButton.OK, MessageBoxImage.Error);
                    BL.Betolt("level4", 70);
                    BL.MegoldasBetolt("level4_megoldas");
                    BL.Ido = BL.AlapIdo;
                }

            }
        }
    }
}
