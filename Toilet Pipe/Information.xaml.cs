﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Toilet_Pipe
{
    /// <summary>
    /// Interaction logic for Information.xaml
    /// </summary>
    public partial class Information : Window
    {
        BusinessLogic BL;

        public Information(BusinessLogic BL)
        {
            InitializeComponent();

            this.BL = BL;
            BL.Jatekos.Nev = "";
            BL.Jatekos.Eletkor = "";
            BL.Jatekos.Nehezseg = "";
            this.DataContext = BL.Jatekos;
            BL.Dt.Stop();
        }

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);

        }

        private void btn_start_Click(object sender, RoutedEventArgs e)
        {
            //jatekos es palya beallitasa
            BL.Jatekos.Pont = 0;
            BL.Betolt("level1", 100);
            BL.Lvl = 1;

            #region nehézség beállítása
            if (rb_easy.IsChecked == true)
            {
                BL.Jatekos.Nehezseg = "easy";
                BL.AlapIdo = 60;
            }
            else if (rb_medium.IsChecked == true)
            {
                BL.Jatekos.Nehezseg = "medium";
                BL.AlapIdo = 45;
            }
            else if (rb_hard.IsChecked == true)
            {
                BL.Jatekos.Nehezseg = "hard";
                BL.AlapIdo = 30;
            }
            else if (rb_verHard.IsChecked == true)
            {
                BL.Jatekos.Nehezseg = "expert";
                BL.AlapIdo = 15;
            }
            #endregion

            
            #region beviteli mező error kezelése
            if (tb_age.Text == "" && tb_name.Text == "")
            {
                MessageBox.Show("Töltsd ki az összes mezőt!", "Hiányzó adatok!", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            else if (tb_age.Text == "")
            {
                MessageBox.Show("Töltsd ki az 'age' mezőt is!", "Hiányzó adat!", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            else if (tb_name.Text == "")
            {
                MessageBox.Show("Töltsd ki a 'name' mezőt is!", "Hiányzó adat!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                PlayingField playingField = new PlayingField(this.BL);
                playingField.ShowDialog();
                this.Close();               
            }
            #endregion

            

           
        }

        private void tb_MouseMove(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Pen;
        }

        private void MouseLeave(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
        }

        private void MouseMove(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Hand;
        }

        private void rb_MouseMove(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Cross;

        }

        private void rb_easy_Checked(object sender, RoutedEventArgs e)
        {
            
        }

        private void rb_medium_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void rb_hard_Checked(object sender, RoutedEventArgs e)
        {
           
        }

        private void rb_verHard_Checked(object sender, RoutedEventArgs e)
        {
  
        }
    }
}
