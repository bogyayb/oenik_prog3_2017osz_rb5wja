﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Toilet_Pipe
{
    public class Player : Bindable
    {
        //fields
        string nev;
        string eletkor;
        int pont;
        string nehezseg;

        //ctor
        public Player()
        {
            this.Nev = "";
            this.Eletkor = "";
            Pont = 0;
            this.nehezseg = "";
        }

        public Player(string nev, string kor)
        {
            this.Nev = nev;
            this.Eletkor = kor;
            Pont = 0;
            this.nehezseg = "";
        }

        //get/set
        public string Nev
        {
            get
            {
                return nev;
            }

            set
            {
                nev = value;
                OPC();
            }
        }

        public int Pont
        {
            get
            {
                return pont;
            }

            set
            {
                pont = value;
                OPC();
            }
        }

        public string Eletkor
        {
            get
            {
                return eletkor;
            }

            set
            {
                eletkor = value;
                OPC();
            }
        }

        public string Nehezseg
        {
            get
            {
                return nehezseg;
            }

            set
            {
                nehezseg = value;
                OPC();
            }
        }
    }
}
