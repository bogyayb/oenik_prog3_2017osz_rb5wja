﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Toilet_Pipe
{
    public class Eredmeny : Bindable
    {
        string elnevezes;
        string szerzettPont;
        string kor;
        string szint;

        public Eredmeny(string nev, string pont, string asd, string sz)
        {
            elnevezes = nev;
            szerzettPont = pont;
            Kor = asd;
            Szint = sz;
        }
        

        public string SzerzettPont
        {
            get
            {
                return szerzettPont;
            }

            set
            {
                szerzettPont = value;
                OPC();
            }
        }

        public string Elnevezes
        {
            get
            {
                return elnevezes;
            }

            set
            {
                elnevezes = value;
            }
        }

        public string Kor
        {
            get
            {
                return kor;
            }

            set
            {
                kor = value;
            }
        }

        public string Szint
        {
            get
            {
                return szint;
            }

            set
            {
                szint = value;
            }
        }
    }
}
