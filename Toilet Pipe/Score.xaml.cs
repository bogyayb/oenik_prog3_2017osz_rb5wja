﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Toilet_Pipe
{
    /// <summary>
    /// Interaction logic for Score.xaml
    /// </summary>
    public partial class Score : Window
    {

        public Score(BusinessLogic BL)
        {
            InitializeComponent();
            
            this.DataContext = BL;
            //BL.Eredmenyek.Add(new Player());
        }

        public Score()
        {
            InitializeComponent();

            

            this.DataContext = this;
        }

        //get/set

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void MouseMove(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Hand;
        }

        private void MouseLeave(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
        }
    }
}
