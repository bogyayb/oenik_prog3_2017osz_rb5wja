﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Toilet_Pipe
{
    public partial class MainWindow : Window
    {
        BusinessLogic BL;

        public MainWindow()
        {
            InitializeComponent();

            this.BL = new BusinessLogic();
            BL.Dt.Stop();
        }

        private void btn_exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_description_Click(object sender, RoutedEventArgs e)
        {
            Description description = new Description();
            description.ShowDialog();
        }

        private void btn_score_Click(object sender, RoutedEventArgs e)
        {
            Score score = new Score(BL);
            score.ShowDialog();
        }

        private void btn_play_Click(object sender, RoutedEventArgs e)
        {
            Information information = new Information(this.BL);
            information.ShowDialog();
        }

        private void btn_play_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Information information = new Information(this.BL);
            information.ShowDialog();
        }

        private void MouseMove(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Hand;
        }

        private void MouseLeave(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
        }
    }
}
