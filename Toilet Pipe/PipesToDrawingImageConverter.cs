﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Toilet_Pipe
{
    class PipesToDrawingImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //value: Pipe[,]
            //return: DrawingImage

            Pipe[,] tabla = (Pipe[,])value;
            DrawingGroup drawingGroup = new DrawingGroup();
            PipeToRectangleGeometryConverter pipeRectangleGeometry = new PipeToRectangleGeometryConverter();

            for (int i = 0; i < tabla.GetLength(0); i++)
            {
                for (int j = 0; j < tabla.GetLength(1); j++)
                {
                    if (tabla[i,j] != null)
                    {
                        RectangleGeometry aktPipeRectGeo = (RectangleGeometry)pipeRectangleGeometry.Convert(tabla[i,j], null, null, null);
                        GeometryDrawing aktMezoGeometryDrawing = new GeometryDrawing(tabla[i,j].AktulisKep, null, aktPipeRectGeo);
                        drawingGroup.Children.Add(aktMezoGeometryDrawing);
                    }
                    
                }
            }

            DrawingImage drawingImage = new DrawingImage();
            drawingImage.Drawing = drawingGroup;

            return drawingImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
