﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace Toilet_Pipe
{
    public class BusinessLogic : Bindable
    {
        //fields
        Pipe[,] tabla;
        Player jatekos;
        ObservableCollection<Eredmeny> eredmenyek;
        DispatcherTimer dt;
        int ido = 180;
        int lvl = 0;
        int alapIdo = new int();

        //megoldások
        List<MegoldasResz> megoldas = new List<MegoldasResz>();

        //ctor
        public BusinessLogic()
        {
            this.eredmenyek = new ObservableCollection<Eredmeny>();
            Betolt("level1", 100);
            MegoldasBetolt("level1_megoldas");
            lvl++;

            //tabla = new Pipe[6, 5];
            this.jatekos = new Player();

            #region pálya létrehozása
            //tabla = new Pipe[1,2];

            //tabla[0, 0] = new Pipe(PipeTipus.egyenes, 0 * Pipe.szelesseg, 0 * Pipe.magassag);
            //tabla[0, 1] = new Pipe(PipeTipus.ferde, 0 * Pipe.szelesseg, 1 * Pipe.magassag);
            //tabla[0, 2] = new Pipe(PipeTipus.egyenes, 0 * Pipe.szelesseg, 2 * Pipe.magassag);
            //tabla[0, 3] = new Pipe(PipeTipus.egyenes, 0 * Pipe.szelesseg, 3 * Pipe.magassag);
            //tabla[0, 4] = new Pipe(PipeTipus.dupla, 0 * Pipe.szelesseg, 4 * Pipe.magassag);

            //tabla[1, 0] = new Pipe(PipeTipus.egyenes, 1 * Pipe.szelesseg, 0 * Pipe.magassag);
            //tabla[1, 1] = new Pipe(PipeTipus.ferde, 1 * Pipe.szelesseg, 1 * Pipe.magassag);
            //tabla[1, 2] = new Pipe(PipeTipus.egyenes, 1 * Pipe.szelesseg, 2 * Pipe.magassag);
            //tabla[1, 3] = new Pipe(PipeTipus.egyenes, 1 * Pipe.szelesseg, 3 * Pipe.magassag);
            //tabla[1, 4] = new Pipe(PipeTipus.dupla, 1 * Pipe.szelesseg, 4 * Pipe.magassag);

            //tabla[2, 0] = new Pipe(PipeTipus.egyenes, 2 * Pipe.szelesseg, 0 * Pipe.magassag);
            //tabla[2, 1] = new Pipe(PipeTipus.dupla, 2 * Pipe.szelesseg, 1 * Pipe.magassag);
            //tabla[2, 2] = new Pipe(PipeTipus.ferde, 2 * Pipe.szelesseg, 2 * Pipe.magassag);
            //tabla[2, 3] = new Pipe(PipeTipus.egyenes, 2 * Pipe.szelesseg, 3 * Pipe.magassag);
            //tabla[2, 4] = new Pipe(PipeTipus.egyenes, 2 * Pipe.szelesseg, 4 * Pipe.magassag);

            //tabla[3, 0] = new Pipe(PipeTipus.egyenes, 3 * Pipe.szelesseg, 0 * Pipe.magassag);
            //tabla[3, 1] = new Pipe(PipeTipus.egyenes, 3 * Pipe.szelesseg, 1 * Pipe.magassag);
            //tabla[3, 2] = new Pipe(PipeTipus.dupla, 3 * Pipe.szelesseg, 2 * Pipe.magassag);
            //tabla[3, 3] = new Pipe(PipeTipus.egyenes, 3 * Pipe.szelesseg, 3 * Pipe.magassag);
            //tabla[3, 4] = new Pipe(PipeTipus.egyenes, 3 * Pipe.szelesseg, 4 * Pipe.magassag);

            //tabla[4, 0] = new Pipe(PipeTipus.ferde, 4 * Pipe.szelesseg, 0 * Pipe.magassag);
            //tabla[4, 1] = new Pipe(PipeTipus.egyenes, 4 * Pipe.szelesseg, 1 * Pipe.magassag);
            //tabla[4, 2] = new Pipe(PipeTipus.dupla, 4 * Pipe.szelesseg, 2 * Pipe.magassag);
            //tabla[4, 3] = new Pipe(PipeTipus.ferde, 4 * Pipe.szelesseg, 3 * Pipe.magassag);
            //tabla[4, 4] = new Pipe(PipeTipus.egyenes, 4 * Pipe.szelesseg, 4 * Pipe.magassag);

            //tabla[5, 0] = new Pipe(PipeTipus.egyenes, 5 * Pipe.szelesseg, 0 * Pipe.magassag);
            //tabla[5, 1] = new Pipe(PipeTipus.egyenes, 5 * Pipe.szelesseg, 1 * Pipe.magassag);
            //tabla[5, 2] = new Pipe(PipeTipus.dupla, 5 * Pipe.szelesseg, 2 * Pipe.magassag);
            //tabla[5, 3] = new Pipe(PipeTipus.egyenes, 5 * Pipe.szelesseg, 3 * Pipe.magassag);
            //tabla[5, 4] = new Pipe(PipeTipus.egyenes, 5 * Pipe.szelesseg, 4 * Pipe.magassag);
            #endregion

            //Betolt("level1", 70);

            //
            Dt = new DispatcherTimer();
            Dt.Interval = new TimeSpan(0, 0, 0, 1);
            Dt.Tick += Telik; ;
        }

        //get/set
        public Pipe[,] Tabla
        {
            get
            {
                return tabla;
            }

            set
            {
                tabla = value;
                OPC();
            }
        }

        public Player Jatekos
        {
            get
            {
                return jatekos;
            }

            set
            {
                jatekos = value;
                OPC();
            }
        }

        public ObservableCollection<Eredmeny> Eredmenyek
        {
            get
            {
                return eredmenyek;
            }

            set
            {
                eredmenyek = value;
                OPC();
            }
        }

        public int Ido
        {
            get
            {
                return ido;
            }

            set
            {
                ido = value;
                OPC();
            }
        }

        public DispatcherTimer Dt
        {
            get
            {
                return dt;
            }

            set
            {
                dt = value;
                OPC();
            }
        }

        public int Lvl
        {
            get
            {
                return lvl;
            }

            set
            {
                lvl = value;
                OPC();
            }
        }

        public int AlapIdo
        {
            get
            {
                return alapIdo;
            }

            set
            {
                alapIdo = value;
                OPC();
            }
        }

        //methods
        public void Kattintas(double mouseX, double mouseY)
        {
            for (int x = 0; x < tabla.GetLength(0); x++)
            {
                for (int y = 0; y < tabla.GetLength(1); y++)
                {
                    //itt végignézzük, hogy épp belekattintottunk-e valamelyik csőbe, és ha igen akkor azt forgatjuk
                    if (tabla[x,y].BeleKattintottE(mouseX, mouseY))
                    {
                        tabla[x, y].Forgat();
                        //összekötöttek elemzése
                        if (tabla[x, y].Tipus == PipeTipus.osszekotott1 || tabla[x, y].Tipus == PipeTipus.osszekotott2)
                        {
                            for (int x1 = 0; x1 < tabla.GetLength(0); x1++)
                            {
                                for (int y1 = 0; y1 < tabla.GetLength(1); y1++)
                                {
                                    if ((tabla[x1, y1].Tipus == PipeTipus.osszekotott1 || tabla[x1, y1].Tipus == PipeTipus.osszekotott2) && (x != x1 && y1 != y))
                                    {
                                        tabla[x1, y1].Forgat();
                                    }
                                }
                            }

                        }

                        //pontozás
                        if (tabla[x,y].Tipus == PipeTipus.rogzitett)
                        {
                            jatekos.Pont += 10;
                        }
                        else if (tabla[x,y].Tipus !=  PipeTipus.wc && tabla[x, y].Tipus != PipeTipus.fedlap && tabla[x, y].Tipus != PipeTipus.ures)
                        {
                            Jatekos.Pont++;
                        }

                        OPC("Tabla");
                    }
                }
            }
        }

        public void Betolt(string fájlnév, int meret)
        {
            Pipe.Szelesseg = meret;
            Pipe.Magassag = meret;

            var path = Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Adatok/" + fájlnév + ".txt");
            string[] fajlsorai = File.ReadAllLines(path);

            int palyaSzelesseg = 0;
            int palyaMagassag = 0;

            foreach (string item in fajlsorai)
            {
                palyaSzelesseg = item.Length;
                palyaMagassag++;
            }

            fajlsorai = File.ReadAllLines(path);
            tabla = new Pipe[palyaMagassag, palyaSzelesseg];

            for (int i = 0; i < palyaMagassag; i++)
            {
                for (int j = 0; j < palyaSzelesseg; j++)
                {
                    PipeTipus tipus = PipeTipus.egyenes;

                    #region típus kiválasztása
                    if (fajlsorai[i][j] == 'e')
                        tipus = PipeTipus.egyenes;
                    if (fajlsorai[i][j] == 'f')
                        tipus = PipeTipus.ferde;
                    if (fajlsorai[i][j] == 'd')
                        tipus = PipeTipus.dupla;
                    if (fajlsorai[i][j] == 'w')
                        tipus = PipeTipus.wc;
                    if (fajlsorai[i][j] == 'u')
                        tipus = PipeTipus.ures;
                    if (fajlsorai[i][j] == 'l')
                        tipus = PipeTipus.fedlap;
                    if (fajlsorai[i][j] == 'h')
                        tipus = PipeTipus.harmas;
                    if (fajlsorai[i][j] == 'r')
                        tipus = PipeTipus.rogzitett;
                    if (fajlsorai[i][j] == 'k')
                        tipus = PipeTipus.lyukas;
                    if (fajlsorai[i][j] == 'i')
                        tipus = PipeTipus.osszekotott1;
                    if (fajlsorai[i][j] == 'o')
                        tipus = PipeTipus.osszekotott2;
                    #endregion

                    tabla[i, j] = new Pipe(tipus, j * Pipe.Szelesseg, i * Pipe.Magassag);
                }
            }

            OPC("Tabla");

        }

        private void Telik(object sender, EventArgs e)
        {
            Ido--;
            if (ido == 0)
            {
                if (Lvl == 1)
                {
                    MessageBox.Show("Kezdheted előről a pályát, máskor gyorsabb legyél! :(", "Lejárt az idő!", MessageBoxButton.OK, MessageBoxImage.Error);
                    Betolt("level1", 100);
                    MegoldasBetolt("level1_megoldas");
                    Ido = AlapIdo;
                }
                else if (Lvl == 2)
                {
                    MessageBox.Show("Kezdheted előről a pályát, máskor gyorsabb legyél! :(", "Lejárt az idő!", MessageBoxButton.OK, MessageBoxImage.Error);
                    Betolt("level2", 90);
                    MegoldasBetolt("level2_megoldas");
                    Ido = AlapIdo;
                }
                else if (Lvl == 3)
                {
                    MessageBox.Show("Kezdheted előről a pályát, máskor gyorsabb legyél! :(", "Lejárt az idő!", MessageBoxButton.OK, MessageBoxImage.Error);
                    Betolt("level3", 80);
                    MegoldasBetolt("level3_megoldas");
                    Ido = AlapIdo;
                }
                else if (Lvl == 4)
                {
                    MessageBox.Show("Kezdheted előről a pályát, máskor gyorsabb legyél! :(", "Lejárt az idő!", MessageBoxButton.OK, MessageBoxImage.Error);
                    Betolt("level4", 70);
                    MegoldasBetolt("level4_megoldas");
                    Ido = AlapIdo;
                }
            }
        }
        
        public void MegoldasBetolt(string fájlnév)
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Adatok/" + fájlnév + ".txt");

            string[] fajlsorai = File.ReadAllLines(path);

            megoldas = new List<MegoldasResz>();

            foreach (string item in fajlsorai)
            {
                int x = int.Parse(item.Split('-')[0].Split(',')[0]);
                int y = int.Parse(item.Split('-')[0].Split(',')[1]);

                string a = item.Split('-')[1];
                List<int> allasok = new List<int>();

                for (int i = 0; i < a.Split(',').Length; i++)
                {
                    allasok.Add(int.Parse( a.Split(',')[i]));
                }

                megoldas.Add(new MegoldasResz(x, y, allasok));
            }
        }

        public bool MegoldasEllenorzes()
        {
            int joMegoldasok = 0;

            foreach (MegoldasResz item in megoldas)
            {
                for (int i = 0; i < item.AdottAllasok.Count; i++)
                {
                    if (Tabla[item.X, item.Y].AktSzam == (item.AdottAllasok[i] - 1))
                    {
                        joMegoldasok++;
                    }
                }
            }

            return joMegoldasok == megoldas.Count;
        }
    }
}
