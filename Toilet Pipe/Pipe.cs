﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Toilet_Pipe
{
    public enum PipeTipus
    {
        egyenes, ferde, dupla, harmas, rogzitett, lyukas, osszekotott1, osszekotott2, wc, ures, fedlap
    }

    public class Pipe : Bindable
    {
        //statics
        private static int szelesseg;
        public static int magassag;
    

        //fields
        Brush[] kepek;
        Brush aktulisKep;
        int aktSzam;
        Point helyzet;
        Point jobbAlso;
        PipeTipus tipus;

        //ctor
        public Pipe(PipeTipus tipus, int x, int y)
        {
            this.Tipus = tipus;
            this.helyzet = new Point(x,y);

            #region egyenes
            if (tipus == PipeTipus.egyenes)
            {
                this.Kepek = new Brush[2];
                Kepek[0] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_egyenes_1.png"));
                Kepek[1] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_egyenes_2.png"));
            }
            #endregion

            #region ferde
            if (tipus == PipeTipus.ferde)
            {
                this.Kepek = new Brush[4];
                Kepek[0] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_ferde_1.png"));
                Kepek[1] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_ferde_2.png"));
                Kepek[2] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures /pipe_ferde_3.png"));
                Kepek[3] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_ferde_4.png"));
            }
            #endregion

            #region dubla
            if (tipus == PipeTipus.dupla)
            {
                this.Kepek = new Brush[2];
                Kepek[0] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_dupla_1.png"));
                Kepek[1] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_dupla_2.png"));
            }
            #endregion

            #region hármas
            if (tipus == PipeTipus.harmas)
            {
                this.Kepek = new Brush[4];
                Kepek[0] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_harmas_1.png"));
                Kepek[1] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_harmas_2.png"));
                Kepek[2] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_harmas_3.png"));
                Kepek[3] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_harmas_4.png"));
            }
            #endregion

            #region rogzitett
            if (tipus == PipeTipus.rogzitett)
            {
                this.Kepek = new Brush[1];
                Kepek[0] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_rogzitett_ferde.png"));
            }
            #endregion

            #region lyukas
            if (tipus == PipeTipus.lyukas)
            {
                this.Kepek = new Brush[2];
                Kepek[0] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_lyukas_1.png"));
                Kepek[1] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_lyukas_2.png"));
            }
            #endregion
                        
            #region osszekotott1
            if (tipus == PipeTipus.osszekotott1)
            {
                this.Kepek = new Brush[4];
                Kepek[0] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_osszekotott1_1.png"));
                Kepek[1] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_osszekotott1_2.png"));
                Kepek[2] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_osszekotott1_3.png"));
                Kepek[3] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_osszekotott1_4.png"));
            }
            #endregion

            #region osszekotott2
            if (tipus == PipeTipus.osszekotott2)
            {
                this.Kepek = new Brush[2];
                Kepek[0] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_osszekotott2_1.png"));
                Kepek[1] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/pipe_osszekotott2_2.png"));
            }
            #endregion

            #region ures
            if (tipus == PipeTipus.ures)
            {
                this.Kepek = new Brush[1];
                Kepek[0] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/ures.png"));
            }
            #endregion

            #region wc
            if (tipus == PipeTipus.wc)
            {
                this.Kepek = new Brush[1];
                Kepek[0] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/toilet.png"));
            }
            #endregion

            #region fedlap
            if (tipus == PipeTipus.fedlap)
            {
                this.Kepek = new Brush[1];
                Kepek[0] = GetBrush(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Pictures/fedlap.png"));
            }
            #endregion

            this.AktSzam = 0;
            this.AktulisKep = Kepek[AktSzam];

        }

        //get/set
        public Brush[] Kepek
        {
            get
            {
                return kepek;
            }

            set
            {
                kepek = value;
                OPC();
            }
        }

        public Brush AktulisKep
        {
            get
            {
                return aktulisKep;
            }

            set
            {
                aktulisKep = value;
                OPC();
            }
        }

        public int AktSzam
        {
            get
            {
                return aktSzam;
            }

            set
            {
                aktSzam = value;
                OPC();
            }
        }

        public PipeTipus Tipus
        {
            get
            {
                return tipus;
            }

            set
            {
                tipus = value;
                OPC();
            }
        }

        public Point Helyzet
        {
            get
            {
                return helyzet;
            }

            set
            {
                helyzet = value;
                OPC();
            }
        }

        public static int Magassag
        {
            get
            {
                return magassag;
            }

            set
            {
                magassag = value;
            }
        }

        public static int Szelesseg
        {
            get
            {
                return szelesseg;
            }

            set
            {
                szelesseg = value;
            }
        }

        //methods
        public void Forgat()
        {
            if ((aktSzam + 1) < Kepek.Length)
            {
                AktSzam++;
                AktulisKep = Kepek[AktSzam];
            }
            else
            {
                AktSzam = 0;
                AktulisKep = Kepek[AktSzam];
            }
        }

        public bool BeleKattintottE(double x, double y)
        {
            bool xOK = false;
            bool yOK = false;

            if (x > this.Helyzet.X && x < this.helyzet.X + Pipe.Szelesseg)
            {
                xOK = true;
            }
            if (y > this.Helyzet.Y && y < this.helyzet.Y + Pipe.Magassag)
            {
                yOK = true;
            }

            if (yOK == true && xOK == true)
            {
                return true;
            }
            else
                return false;

        }

        //just teszt
        public bool BeleKattintottETeszt(double x, double y)
        {

            bool xOK = false;
            bool yOK = false;

            if (x > this.Helyzet.X * Szelesseg && x < this.helyzet.X * Szelesseg + Szelesseg)
            {
                xOK = true;
            }
            if (y > this.Helyzet.Y * Magassag && y < this.helyzet.Y * Magassag + Magassag)
            {
                yOK = true;
            }

            if (yOK == true && xOK == true)
            {
                return true;
            }
            else
                return false;

        }

        //helper methods
        protected Brush GetBrush(string fajlNev)
        {
            //kepEcset letrehozasa
            ImageBrush kepEcset = new ImageBrush(new BitmapImage(new Uri(fajlNev, UriKind.Relative)));

            //csemés rajzoláshoz
            kepEcset.TileMode = TileMode.Tile;
            kepEcset.Viewport = new Rect(0, 0, Szelesseg, Magassag);
            kepEcset.ViewportUnits = BrushMappingMode.Absolute;

            return kepEcset;
        }
    }
}
